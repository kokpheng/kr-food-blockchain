cat << "EOF"
                          Composer Multi Org Generator
EOF

ARCH=$(uname -s | grep Darwin)
echo "Removing existing cards if any"
rm -rf $HOME/.composer
rm -rf /tmp/composer
echo "Creating temporary directories for org 1"
mkdir -p /tmp/composer/hdkia
mkdir -p /tmp/composer/hdjk
mkdir -p /tmp/composer/ewha
mkdir -p /tmp/composer/halla
mkdir -p /tmp/composer/sejin
mkdir -p /tmp/composer/sangsin
mkdir -p /tmp/composer/hkt
mkdir -p /tmp/composer/shinhan
mkdir -p /tmp/composer/lg

echo "Pasting Org hdkia certificates in tmp/composer/hdkia"
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/hdkia.blockchain.fta.com/peers/peer0.hdkia.blockchain.fta.com/tls/ca.crt > /tmp/composer/hdkia/ca-hdkia.txt

echo "Pasting Org hdjk certificates in tmp/composer/hdjk"
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/hdjk.blockchain.fta.com/peers/peer0.hdjk.blockchain.fta.com/tls/ca.crt > /tmp/composer/hdjk/ca-hdjk.txt

echo "Pasting Org ewha certificates in tmp/composer/ewha"
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/ewha.blockchain.fta.com/peers/peer0.ewha.blockchain.fta.com/tls/ca.crt > /tmp/composer/ewha/ca-ewha.txt

echo "Pasting Org halla certificates in tmp/composer/halla"
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/halla.blockchain.fta.com/peers/peer0.halla.blockchain.fta.com/tls/ca.crt > /tmp/composer/halla/ca-halla.txt

echo "Pasting Org sejin certificates in tmp/composer/sejin"
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/sejin.blockchain.fta.com/peers/peer0.sejin.blockchain.fta.com/tls/ca.crt > /tmp/composer/sejin/ca-sejin.txt

echo "Pasting Org sangsin certificates in tmp/composer/sangsin"
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/sangsin.blockchain.fta.com/peers/peer0.sangsin.blockchain.fta.com/tls/ca.crt > /tmp/composer/sangsin/ca-sangsin.txt

echo "Pasting Org hkt certificates in tmp/composer/hkt"
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/hkt.blockchain.fta.com/peers/peer0.hkt.blockchain.fta.com/tls/ca.crt > /tmp/composer/hkt/ca-hkt.txt

echo "Pasting Org shinhan certificates in tmp/composer/shinhan"
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/shinhan.blockchain.fta.com/peers/peer0.shinhan.blockchain.fta.com/tls/ca.crt > /tmp/composer/shinhan/ca-shinhan.txt

echo "Pasting Org lg certificates in tmp/composer/lg"
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/lg.blockchain.fta.com/peers/peer0.lg.blockchain.fta.com/tls/ca.crt > /tmp/composer/lg/ca-lg.txt

# Orderer
echo "Pasting Orderer certificates in tmp/composer"
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/ordererOrganizations/blockchain.fta.com/orderers/orderer.blockchain.fta.com/tls/ca.crt > /tmp/composer/ca-orderer.txt



echo "Creating the connection profile file"
cp connectionProfile.json  /tmp/composer/kfbn-network.json


echo "Adding the certificates in connection profile"
replacementhdkia="/tmp/composer/hdkia/ca-hdkia.txt"
replacementhdjk="/tmp/composer/hdjk/ca-hdjk.txt"
replacementewha="/tmp/composer/ewha/ca-ewha.txt"
replacementhalla="/tmp/composer/halla/ca-halla.txt"
replacementsejin="/tmp/composer/sejin/ca-sejin.txt"
replacementsangsin="/tmp/composer/sangsin/ca-sangsin.txt"
replacementhkt="/tmp/composer/hkt/ca-hkt.txt"
replacementshinhan="/tmp/composer/shinhan/ca-shinhan.txt"
replacementlg="/tmp/composer/lg/ca-lg.txt"

replacementOrderer="/tmp/composer/ca-orderer.txt"

file="/tmp/composer/kfbn-network.json"
hdkia="/tmp/composer/kfbnhdkia.json"
hdjk="/tmp/composer/kfbnhdjk.json"
ewha="/tmp/composer/kfbnewha.json"
halla="/tmp/composer/kfbnhalla.json"
sejin="/tmp/composer/kfbnsejin.json"
sangsin="/tmp/composer/kfbnsangsin.json"
hkt="/tmp/composer/kfbnhkt.json"
shinhan="/tmp/composer/kfbnshinhan.json"
lg="/tmp/composer/kfbnlg.json"

final="/tmp/composer/kfbnFinal.json"

# Replace text
if [ "$ARCH" == "Darwin" ]; then
    sed -i "" 's/\\n/±/g' $replacementhdkia
    sed -i "" 's/\\n/±/g' $replacementhdjk
    sed -i "" 's/\\n/±/g' $replacementewha
    sed -i "" 's/\\n/±/g' $replacementhalla
    sed -i "" 's/\\n/±/g' $replacementsejin
    sed -i "" 's/\\n/±/g' $replacementsangsin
    sed -i "" 's/\\n/±/g' $replacementhkt
    sed -i "" 's/\\n/±/g' $replacementshinhan
    sed -i "" 's/\\n/±/g' $replacementlg
    sed -i "" 's/\\n/±/g' $replacementOrderer
else
    sed -i 's/\\n/±/g' $replacementhdkia
    sed -i 's/\\n/±/g' $replacementhdjk
    sed -i 's/\\n/±/g' $replacementewha
    sed -i 's/\\n/±/g' $replacementhalla
    sed -i 's/\\n/±/g' $replacementsejin
    sed -i 's/\\n/±/g' $replacementsangsin
    sed -i 's/\\n/±/g' $replacementhkt
    sed -i 's/\\n/±/g' $replacementshinhan
    sed -i 's/\\n/±/g' $replacementlg
    sed -i 's/\\n/±/g' $replacementOrderer
fi

echo "replacement"+ $replacementhdkia

sed -e "s@INSERT_hdkia_CA_CERT@$(cat $replacementhdkia)@g" $file > $hdkia
sed -e "s@INSERT_hdjk_CA_CERT@$(cat $replacementhdjk)@g" $hdkia > $hdjk
sed -e "s@INSERT_ewha_CA_CERT@$(cat $replacementewha)@g" $hdjk > $ewha
sed -e "s@INSERT_halla_CA_CERT@$(cat $replacementhalla)@g" $ewha > $halla
sed -e "s@INSERT_sejin_CA_CERT@$(cat $replacementsejin)@g" $halla > $sejin
sed -e "s@INSERT_sangsin_CA_CERT@$(cat $replacementsangsin)@g" $sejin > $sangsin
sed -e "s@INSERT_hkt_CA_CERT@$(cat $replacementhkt)@g" $sangsin > $hkt
sed -e "s@INSERT_shinhan_CA_CERT@$(cat $replacementshinhan)@g" $hkt > $shinhan
sed -e "s@INSERT_lg_CA_CERT@$(cat $replacementlg)@g" $shinhan > $lg

sed -e "s@INSERT_ORDERER_CA_CERT@$(cat $replacementOrderer)@g" $lg > $final

if [ "$ARCH" == "Darwin" ]; then
    sed -i "" 's/\\n/±/g' $final
else
    sed -i 's/\±/\\n/g' $final
fi

rm $hdkia $hdjk $ewha $halla $sejin $sangsin $hkt $shinhan $lg

echo $final

echo "Creating connection profile for hdkia"
cp /tmp/composer/kfbnFinal.json /tmp/composer/hdkia/kfbn-network-hdkia.json
ex -sc '5i|    "client": {
        "organization": "HDKia",
        "connection": {
            "timeout": {
                "peer": {
                    "endorser": "300",
                    "eventHub": "300",
                    "eventReg": "300"
                },
                "orderer": "300"
            }
        }
    },' -cx /tmp/composer/hdkia/kfbn-network-hdkia.json

echo "Creating connection profile for hdjk"
cp /tmp/composer/kfbnFinal.json /tmp/composer/hdjk/kfbn-network-hdjk.json
ex -sc '5i|    "client": {
        "organization": "HDJK",
        "connection": {
            "timeout": {
                "peer": {
                    "endorser": "300",
                    "eventHub": "300",
                    "eventReg": "300"
                },
                "orderer": "300"
            }
        }
    },' -cx /tmp/composer/hdjk/kfbn-network-hdjk.json

echo "Creating connection profile for ewha"
cp /tmp/composer/kfbnFinal.json /tmp/composer/ewha/kfbn-network-ewha.json
ex -sc '5i|    "client": {
        "organization": "Ewha",
        "connection": {
            "timeout": {
                "peer": {
                    "endorser": "300",
                    "eventHub": "300",
                    "eventReg": "300"
                },
                "orderer": "300"
            }
        }
    },' -cx /tmp/composer/ewha/kfbn-network-ewha.json

echo "Creating connection profile for halla"
cp /tmp/composer/kfbnFinal.json /tmp/composer/halla/kfbn-network-halla.json
ex -sc '5i|    "client": {
        "organization": "Halla",
        "connection": {
            "timeout": {
                "peer": {
                    "endorser": "300",
                    "eventHub": "300",
                    "eventReg": "300"
                },
                "orderer": "300"
            }
        }
    },' -cx /tmp/composer/halla/kfbn-network-halla.json


echo "Creating connection profile for sejin"
cp /tmp/composer/kfbnFinal.json /tmp/composer/sejin/kfbn-network-sejin.json
ex -sc '5i|    "client": {
        "organization": "Sejin",
        "connection": {
            "timeout": {
                "peer": {
                    "endorser": "300",
                    "eventHub": "300",
                    "eventReg": "300"
                },
                "orderer": "300"
            }
        }
    },' -cx /tmp/composer/sejin/kfbn-network-sejin.json

echo "Creating connection profile for sangsin"
cp /tmp/composer/kfbnFinal.json /tmp/composer/sangsin/kfbn-network-sangsin.json
ex -sc '5i|    "client": {
        "organization": "Sangsin",
        "connection": {
            "timeout": {
                "peer": {
                    "endorser": "300",
                    "eventHub": "300",
                    "eventReg": "300"
                },
                "orderer": "300"
            }
        }
    },' -cx /tmp/composer/sangsin/kfbn-network-sangsin.json

echo "Creating connection profile for hkt"
cp /tmp/composer/kfbnFinal.json /tmp/composer/hkt/kfbn-network-hkt.json
ex -sc '5i|    "client": {
        "organization": "HKT",
        "connection": {
            "timeout": {
                "peer": {
                    "endorser": "300",
                    "eventHub": "300",
                    "eventReg": "300"
                },
                "orderer": "300"
            }
        }
    },' -cx /tmp/composer/hkt/kfbn-network-hkt.json

echo "Creating connection profile for shinhan"
cp /tmp/composer/kfbnFinal.json /tmp/composer/shinhan/kfbn-network-shinhan.json
ex -sc '5i|    "client": {
        "organization": "Shinhan",
        "connection": {
            "timeout": {
                "peer": {
                    "endorser": "300",
                    "eventHub": "300",
                    "eventReg": "300"
                },
                "orderer": "300"
            }
        }
    },' -cx /tmp/composer/shinhan/kfbn-network-shinhan.json

echo "Creating connection profile for lg"
cp /tmp/composer/kfbnFinal.json /tmp/composer/lg/kfbn-network-lg.json
ex -sc '5i|    "client": {
        "organization": "LG",
        "connection": {
            "timeout": {
                "peer": {
                    "endorser": "300",
                    "eventHub": "300",
                    "eventReg": "300"
                },
                "orderer": "300"
            }
        }
    },' -cx /tmp/composer/lg/kfbn-network-lg.json

if [ "$ARCH" == "Darwin" ]; then
    sed -i "" "s/INSERT_CONNECTION_PROFILE/hdkia-fta-network/g" /tmp/composer/hdkia/kfbn-network-hdkia.json
    sed -i "" "s/INSERT_CONNECTION_PROFILE/hdjk-fta-network/g" /tmp/composer/hdjk/kfbn-network-hdjk.json
    sed -i "" "s/INSERT_CONNECTION_PROFILE/ewha-fta-network/g" /tmp/composer/ewha/kfbn-network-ewha.json
    sed -i "" "s/INSERT_CONNECTION_PROFILE/halla-fta-network/g" /tmp/composer/halla/kfbn-network-halla.json
    sed -i "" "s/INSERT_CONNECTION_PROFILE/sejin-fta-network/g" /tmp/composer/sejin/kfbn-network-sejin.json
    sed -i "" "s/INSERT_CONNECTION_PROFILE/sangsin-fta-network/g" /tmp/composer/sangsin/kfbn-network-sangsin.json
    sed -i "" "s/INSERT_CONNECTION_PROFILE/hkt-fta-network/g" /tmp/composer/hkt/kfbn-network-hkt.json
    sed -i "" "s/INSERT_CONNECTION_PROFILE/shinhan-fta-network/g" /tmp/composer/shinhan/kfbn-network-shinhan.json
    sed -i "" "s/INSERT_CONNECTION_PROFILE/lg-fta-network/g" /tmp/composer/lg/kfbn-network-lg.json
else
    sed -i "s/INSERT_CONNECTION_PROFILE/hdkia-fta-network/g" /tmp/composer/hdkia/kfbn-network-hdkia.json
    sed -i "s/INSERT_CONNECTION_PROFILE/hdjk-fta-network/g" /tmp/composer/hdjk/kfbn-network-hdjk.json
    sed -i "s/INSERT_CONNECTION_PROFILE/ewha-fta-network/g" /tmp/composer/ewha/kfbn-network-ewha.json
    sed -i "s/INSERT_CONNECTION_PROFILE/halla-fta-network/g" /tmp/composer/halla/kfbn-network-halla.json
    sed -i "s/INSERT_CONNECTION_PROFILE/sejin-fta-network/g" /tmp/composer/sejin/kfbn-network-sejin.json
    sed -i "s/INSERT_CONNECTION_PROFILE/sangsin-fta-network/g" /tmp/composer/sangsin/kfbn-network-sangsin.json
    sed -i "s/INSERT_CONNECTION_PROFILE/hkt-fta-network/g" /tmp/composer/hkt/kfbn-network-hkt.json
    sed -i "s/INSERT_CONNECTION_PROFILE/shinhan-fta-network/g" /tmp/composer/shinhan/kfbn-network-shinhan.json
    sed -i "s/INSERT_CONNECTION_PROFILE/lg-fta-network/g" /tmp/composer/lg/kfbn-network-lg.json
fi

echo "getting the certificates for administrator of hdkia"
export hdkia=crypto-config/peerOrganizations/hdkia.blockchain.fta.com/users/Admin@hdkia.blockchain.fta.com/msp

cp -p $hdkia/signcerts/A*.pem /tmp/composer/hdkia

cp -p $hdkia/keystore/*_sk /tmp/composer/hdkia

echo "getting the certificates for administrator of hdjk"
export hdjk=crypto-config/peerOrganizations/hdjk.blockchain.fta.com/users/Admin@hdjk.blockchain.fta.com/msp

cp -p $hdjk/signcerts/A*.pem /tmp/composer/hdjk

cp -p $hdjk/keystore/*_sk /tmp/composer/hdjk

echo "getting the certificates for administrator of ewha"
export ewha=crypto-config/peerOrganizations/ewha.blockchain.fta.com/users/Admin@ewha.blockchain.fta.com/msp

cp -p $ewha/signcerts/A*.pem /tmp/composer/ewha

cp -p $ewha/keystore/*_sk /tmp/composer/ewha

echo "getting the certificates for administrator of halla"
export halla=crypto-config/peerOrganizations/halla.blockchain.fta.com/users/Admin@halla.blockchain.fta.com/msp

cp -p $halla/signcerts/A*.pem /tmp/composer/halla

cp -p $halla/keystore/*_sk /tmp/composer/halla

echo "getting the certificates for administrator of sejin"
export sejin=crypto-config/peerOrganizations/sejin.blockchain.fta.com/users/Admin@sejin.blockchain.fta.com/msp

cp -p $sejin/signcerts/A*.pem /tmp/composer/sejin

cp -p $sejin/keystore/*_sk /tmp/composer/sejin

echo "getting the certificates for administrator of sangsin"
export sangsin=crypto-config/peerOrganizations/sangsin.blockchain.fta.com/users/Admin@sangsin.blockchain.fta.com/msp

cp -p $sangsin/signcerts/A*.pem /tmp/composer/sangsin

cp -p $sangsin/keystore/*_sk /tmp/composer/sangsin

echo "getting the certificates for administrator of hkt"
export hkt=crypto-config/peerOrganizations/hkt.blockchain.fta.com/users/Admin@hkt.blockchain.fta.com/msp

cp -p $hkt/signcerts/A*.pem /tmp/composer/hkt

cp -p $hkt/keystore/*_sk /tmp/composer/hkt

echo "getting the certificates for administrator of shinhan"
export shinhan=crypto-config/peerOrganizations/shinhan.blockchain.fta.com/users/Admin@shinhan.blockchain.fta.com/msp

cp -p $shinhan/signcerts/A*.pem /tmp/composer/shinhan

cp -p $shinhan/keystore/*_sk /tmp/composer/shinhan

echo "getting the certificates for administrator of lg"
export lg=crypto-config/peerOrganizations/lg.blockchain.fta.com/users/Admin@lg.blockchain.fta.com/msp

cp -p $lg/signcerts/A*.pem /tmp/composer/lg

cp -p $lg/keystore/*_sk /tmp/composer/lg

echo "Copy an endorsement policy file"
cp endorsement-policy.json  /tmp/composer/endorsement-policy.json