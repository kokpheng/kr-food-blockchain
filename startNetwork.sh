# On master
yes | ./byfn.sh down && yes | docker volume prune && yes | ./byfn.sh generate

# for Worker1
ssh kp@worker1 "cd ~/fta-blockchain-network && yes | ./byfn.sh down && yes | docker volume prune";

# for Worker2
ssh kp@worker2 "cd ~/fta-blockchain-network && yes | ./byfn.sh down && yes | docker volume prune";

# copy file
scp -r crypto-config channel-artifacts docker-compose-org1.yaml docker-compose-org2.yaml kp@worker1:~/fta-blockchain-network
scp -r crypto-config channel-artifacts docker-compose-org3.yaml docker-compose-org4.yaml kp@worker2:~/fta-blockchain-network

# start master network
yes | ./byfn.sh up -f docker-compose-orderer.yaml


docker exec -it $(docker ps | awk '{print $NF}' | grep cli-school) bash ./scripts/script.sh

./multiOrgInit.sh

ssh kp@worker1 "rm -rf $HOME/.composer && rm -rf /tmp/composer";

scp -r /tmp/composer kp@worker1:/tmp/composer

ssh kp@worker2 "rm -rf $HOME/.composer && rm -rf /tmp/composer";

scp -r /tmp/composer kp@worker2:/tmp/composer
