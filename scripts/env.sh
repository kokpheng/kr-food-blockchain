# Obtain the OS and Architecture string that will be used to select the correct
# native binaries for your platform, e.g., darwin-amd64 or linux-amd64
OS_ARCH=$(echo "$(uname -s | tr '[:upper:]' '[:lower:]' | sed 's/mingw64_nt.*/windows/')-$(uname -m | sed 's/x86_64/amd64/g')" | awk '{print tolower($0)}')

# PC hostname
export MASTER_HOSTNAME="master"
export PC1_HOSTNAME="worker1"
export PC2_HOSTNAME="worker2"

# Docker Properties
export SWARM_NETWORK="nipa_network"
export DOCKER_STACK="nipa"

export COMPOSE_PROJECT_NAME=nipa

# use golang as the default language for chaincode
LANGUAGE=golang

# default image tag
export IMAGE_TAG="1.2.1"
export IMAGETAG="1.2.1"

# timeout duration - the duration the CLI should wait for a response from
# another container before giving up
export CLI_TIMEOUT=10

# default for delay between commands
export CLI_DELAY=3

export VERBOSE=false