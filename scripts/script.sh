#!/bin/bash

echo
echo " ____    _____      _      ____    _____ "
echo "/ ___|  |_   _|    / \    |  _ \  |_   _|"
echo "\___ \    | |     / _ \   | |_) |   | |  "
echo " ___) |   | |    / ___ \  |  _ <    | |  "
echo "|____/    |_|   /_/   \_\ |_| \_\   |_|  "
echo
echo "Build your first network (BYFN) end-to-end test"
echo

echo "Channel name : "$CHANNEL_NAME

# import utils
. scripts/env.sh
. scripts/nipa-env.sh
. scripts/utils.sh

createChannel() {
	setGlobals 0 "school"

	if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
                set -x
		peer channel create -o orderer.blockchain.nipa:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx >&log.txt
		res=$?
                set +x
	else
				set -x
		peer channel create -o orderer.blockchain.nipa:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
		res=$?
				set +x
	fi
	cat log.txt
	verifyResult $res "Channel creation failed"

	echo "===================== Channel '$CHANNEL_NAME' created ===================== "
	echo
}

joinChannel () {
	for org in "${ORGANIZATIONS_NAME[@]}"; do
	    for peer in 0; do
			joinChannelWithRetry $peer "$org"
			echo "===================== peer${peer}.${org} joined channel '$CHANNEL_NAME' ===================== "
			sleep $CLI_DELAY
			echo
	    done
	done
}

## Create channel
echo "Creating channel..."
createChannel

## Join all the peers to the channel
echo "Having all peers join the channel..."
joinChannel

## Set the anchor peers for each org in the channel
for org in "${ORGANIZATIONS_NAME[@]}"; do
	echo "Updating anchor peers for ${org}..."
	for peer in 0; do
		updateAnchorPeers $peer "$org"
		sleep $CLI_DELAY
		echo
	done
done

## Install chaincode on peer0.org1 and peer0.org2
for org in "${ORGANIZATIONS_NAME[@]}"; do
	for peer in 0; do
	echo "Installing chaincode on peer${peer}.${org}..."
		installChaincode $peer "$org"
		sleep $CLI_DELAY
		echo
	done
done


# Instantiate chaincode on peer0.hdkia
echo "Instantiating chaincode on peer0.school..."
instantiateChaincode 0 "school"

# Query chaincode on peer0.org1
echo "Querying chaincode on peer0.food..."
chaincodeQuery 0 "school" 100

# # # Invoke chaincode on peer0.org1 and peer0.org2
# echo "Sending invoke transaction on peer0.school peer0.food..."
# chaincodeInvoke 0 "school" 0 "food"

# # ## Install chaincode on peer1.org2
# echo "Installing chaincode on peer1.org2..."
# installChaincode 1 2

# # # Query on chaincode on peer1.org2, check if the result is 90
# echo "Querying chaincode on peer0.school..."
# chaincodeQuery 0 "school" 100

echo
echo "========= All GOOD, BYFN execution completed =========== "
echo

echo
echo " _____   _   _   ____   "
echo "| ____| | \ | | |  _ \  "
echo "|  _|   |  \| | | | | | "
echo "| |___  | |\  | | |_| | "
echo "|_____| |_| \_| |____/  "
echo

exit 0
